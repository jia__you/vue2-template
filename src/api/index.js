import request from '@/utils/request'

export function getLogin() {
  return request({
    url: '/login',
  })
}
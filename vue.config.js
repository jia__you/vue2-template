const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      // 配置代理
      '/api': {
        target: 'http://localhost:3000',  // 设置你想要代理的目标主机
        changeOrigin: true,  // 开启跨域
        pathRewrite: {
          '^/api': ''  // 将请求地址中的 /api 前缀替换为空
        }
      }
    }
  }
})

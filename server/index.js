//导入express
const express = require('express')
//创建web服务器
const app=express()
// 使用 Express 内置中间件来替代 body-parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
const router=require('./router');
app.use('/',router)
require('./middle/mysql/index')
require('./addData/index')
app.use(express.json());
// 通过ap.listen进行服务器的配置，并启动服务器，接收两个配置参数，一个是对应的端口号，一个是启动成功的回调函数
app.listen(3000,()=>{
    console.log('express服务器启动成功:http://localhost:3000');
})

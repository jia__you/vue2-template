const mysql=require('mysql')
// 创建数据库连接
let connection=mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'123456',
    database:'city'
})
// 连接
connection.connect(function(err){
    if(err){
        console.log(err)
    }else{
        console.log('connect success!')
    }
})

//关闭连接
// connection.end()
module.exports=connection
